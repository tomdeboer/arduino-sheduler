#include "Sheduler.h"

Sheduler::Sheduler()
{
	first_task = NULL;
}

Task* Sheduler::addTask(void (*fn)(Task*), uint16_t interval, boolean run_now ){

	// Allocate memory for new task.
	Task* new_task = (Task*) malloc(sizeof(Task));
	
	// Init task
	new_task->function    = fn;
	new_task->interval    = interval;
	new_task->idle_cycles = run_now ? 0 : interval;
	new_task->next        = NULL;
	
	if ( first_task != NULL){
		// Remember last_task in this variable.
		Task* last_task; 
		
		// Loop towards the last task.
		for(last_task = first_task; last_task->next != NULL; last_task = last_task->next);
		
		// Add last task to the task chain.
		last_task->next = new_task;
	}else{
		first_task = new_task;
	}
}
void Sheduler::tick(){
	// Loop through all tasks and decrediment their idle_cycles
	for(Task* t = first_task; t != NULL; t = t->next)
		if ( t->idle_cycles )
			t->idle_cycles -= 1;
}

void Sheduler::run(){
	
	for(Task* t = first_task; t != NULL; t = t->next){
		// Skip tasks that have idle cycles left to wait for.
		if ( t->idle_cycles ) continue;

		// Set the idle_cycles for the next time before running.
		// This way, the task can set it's own idle_cycles if it wants to.
		t->idle_cycles = t->interval;
		// Call the task's function.
		t->function( t );
	}
}