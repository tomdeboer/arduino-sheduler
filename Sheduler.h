#ifndef Sheduler_H
#define Sheduler_H

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif


typedef struct Task {
    void (*function)(struct Task*);
    uint16_t interval;
    uint16_t idle_cycles;
    Task* next;
} Task;


class Sheduler {
	public:
		Sheduler();

		Task* addTask(void (*fn)(Task*), uint16_t interval, boolean run_now = false);
		void tick();
		void run();

	protected:
		Task* first_task;

};
#endif
